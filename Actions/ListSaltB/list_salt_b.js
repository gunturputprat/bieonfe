import Api from "../../Services/Api";

const api = new Api();

export default (data) => dispatch => {
    
    dispatch({
        type: 'LIST_SALT_B',
        data,
        payload:null
    })
    
    api.buy_price(data).then((response)=>{
        
        if(typeof response.data !== 'undefined'){
            dispatch({
                type:"LIST_SALT_B_SUCCESS",
                payload: response
            })
        }else{
            dispatch({
                type:"LIST_SALT_B_ERROR",
                payload:null,
            })
        }
    })
}