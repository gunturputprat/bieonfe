const API_URL = process.env.REACT_APP_API_URI;
const qs = require('qs');

export default class Api {
  async post(url, data, withToken) {
    const headers = withToken
      ? {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
      : {
        "Content-Type": "application/json"
      };
    let response = await fetch(API_URL + url, {
      method: "POST",
      body: JSON.stringify(data),
      headers: headers
    })
      .then(response => {
        return response.json();
      })
      .then(result => {
        return result;
      });
    return response;
  }
  async get(url, data, withToken) {
    console.log(url, data, withToken);
    const headers = withToken
      ? {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
      : {
        "Content-Type": "application/json"
      };
    let response = await fetch(API_URL + url + "?" + qs.stringify(data), {

      headers: headers
    })
      .then(response => {
        return response.json();
      })
      .then(result => {
        return result;
      });
    return response;
  }
  login(data) {
    return this.post("/bieonbe.defuture.tech/auth/login", data);
  }
  register(data) {
    return this.post("/api/v1/register", data);
  }
  input_salt_a(data) {
    return this.get("/bieonbe.defuture.tech/salt/a/input", data, true);
  }
  add_input_salt_a(data) {
    return this.post("/bieonbe.defuture.tech/salt/a/input", data, true);
  }
  update_input_salt_a(id, data) {
    return this.post("/bieonbe.defuture.tech/salt/a/input" + id, data, true);
  }
  remove_input_salt_a(id) {
    return this.delete("/bieonbe.defuture.tech/salt/a/input" + id, {}, true);
  }
  input_salt_b(data) {
    return this.get("/bieonbe.defuture.tech/salt/b/input", data, true);
  }
  add_input_salt_b(data) {
    return this.post("/bieonbe.defuture.tech/salt/b/input", data, true);
  }
  update_input_salt_b(id, data) {
    return this.post("/bieonbe.defuture.tech/salt/b/input" + id, data, true);
  }
  remove_input_salt_b(id) {
    return this.delete("/bieonbe.defuture.tech/salt/b/input" + id, {}, true);
  }
  list_salt_a(data) {
    return this.get("/bieonbe.defuture.tech/salt/a/lisr", data, true);
  }
  add_list_salt_a(data) {
    return this.post("/bieonbe.defuture.tech/salt/a/list", data, true);
  }
  update_list_salt_a(id, data) {
    return this.post("/bieonbe.defuture.tech/salt/a/list" + id, data, true);
  }
  remove_list_salt_a(id) {
    return this.delete("/bieonbe.defuture.tech/salt/a/list" + id, {}, true);
  }
  list_salt_b(data) {
    return this.get("/bieonbe.defuture.tech/salt/b/lisr", data, true);
  }
  add_list_salt_b(data) {
    return this.post("/bieonbe.defuture.tech/salt/b/list", data, true);
  }
  update_list_salt_b(id, data) {
    return this.post("/bieonbe.defuture.tech/salt/b/list" + id, data, true);
  }
  remove_list_salt_b(id) {
    return this.delete("/bieonbe.defuture.tech/salt/b/list" + id, {}, true);
  }
}