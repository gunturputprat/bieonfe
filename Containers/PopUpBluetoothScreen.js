import React, { Component } from 'react';
import { StyleSheet, Image, ImageBackground, Text, View, Dimensions, TextInput, TouchableOpacity } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";

export default class PopUpBluetoothScreen extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <Grid>
                <Row size={13}>
                    <View style={styles.container}>
                        <View style={[styles.buttonGoogle]}>
                            <Row style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                                <Text style={[styles.textbuttonGoogle]}>BIEON is asking to turn on Bluetooth</Text>
                                <Image style={styles.itemGoogleImage} source={require('../assets/icons/retrievedata/bluetoothgray.png')} />
                            </Row>
                            <Row style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                                <TouchableOpacity style={[styles.allowleft]} onPress={() => navigate('RetrieveDataScreen')}>
                                    <Text style={[styles.textbuttonallow]}>Deny</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.allowright]} onPress={() => navigate('SelectedDeviceScreen')}>
                                    <Text style={[styles.textbuttonallow]}>Allow</Text>
                                </TouchableOpacity>
                            </Row>
                        </View>
                    </View>
                </Row>
                <Row>
                    <Col style={[styles.col]}>
                        <Image style={styles.itemMenuImage} source={require('../assets/icons/menubar/homeblue.png')} />
                        <Text style={[styles.textmenu]}>Home</Text>
                    </Col>
                    <Col style={[styles.col]}>
                        <Image style={styles.itemMenuImage} source={require('../assets/icons/menubar/retrieveblue.png')} />
                        <Text style={[styles.textmenu]}>Retrieve Data</Text>
                    </Col>
                    <Col style={[styles.col]}>
                        <Image style={styles.itemMenuImage} source={require('../assets/icons/menubar/datablue.png')} />
                        <Text style={[styles.textmenu]}>View Data</Text>
                    </Col>
                    <Col style={[styles.col]}>
                        <Image style={styles.itemMenuImage} source={require('../assets/icons/menubar/profileblue.png')} />
                        <Text style={[styles.textmenu]}>Profile</Text>
                    </Col>
                    <Col style={[styles.col]}>
                        <Image style={styles.itemMenuImage} source={require('../assets/icons/menubar/settingblue.png')} />
                        <Text style={[styles.textmenu]}>Setting</Text>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

const win = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#b6b6b7'
    },
    logo: {
        width: 45,
        height: 45,
    },
    button: {
        marginTop: -140,
        marginBottom: 120,
        marginRight: -320
    },
    text: {
        color: '#fff',
        fontSize: 16,
        marginTop: 15,
        margin: 15,
        textAlign: 'justify',
        color: '#000',
        fontWeight: '600'
    },
    itemMenuImage: {
        resizeMode: 'contain',
        width: 25,
        height: 25,
        marginTop: 3
    },
    col: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#b6b6b7',
    },
    allowleft: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: -30,
        marginBottom: 10
    },
    allowright: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 80,
        marginBottom: 10
    },
    textbuttonallow: {
        fontSize: 17,
        color: '#000',
        textAlign: 'center',
        marginTop: 20
    },
    textmenu: {
        fontSize: 10,
        marginTop: 5,
        color: '#808080',
    },
    textbluetooth: {
        fontSize: 16,
        margin: 29,
        textAlign: 'center',
        color: '#129cd8',
    },
    buttonGoogle: {
        alignSelf: 'center',
        alignItems: 'center',
        borderRadius: 30,
        width: 300,
        height: 150,
        backgroundColor: '#d9d9d9',
        marginTop: 80
    },
    textbuttonGoogle: {
        fontSize: 20,
        color: '#129cd8',
        fontWeight: '700',
        textAlign: 'center',
        marginLeft: 90,
        margin: 20,
        marginTop: 40
    },
    itemGoogleImage: {
        resizeMode: 'contain',
        width: 50,
        height: 50,
        marginLeft: -8,
        marginRight: 50,
        marginTop: 30
    },
});
