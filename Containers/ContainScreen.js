import React, { Component } from 'react';
import { StyleSheet, Image, ImageBackground, Text, View, Dimensions, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";

export default class ContainScreen extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <Grid>
                <Row size={13}>
                    <View style={styles.container}>
                        <View style={styles.itemContainer}>
                            <Image style={styles.itemIconImage} source={require('../assets/icons/retrievedata/device.png')} />
                            <Text style={styles.itemText}>BIEON-001</Text>
                        </View>
                        <TouchableOpacity style={[styles.button]} onPress={() => navigate('PopUpBluetoothScreen')}>
                            <Image style={[styles.logo]} source={require('../assets/icons/retrievedata/bluetoothblue.png')}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.buttonGoogle]} onPress={() => navigate('ContainDetailNaclScreen')}>
                            <Row>
                                <Col size={4} style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                    <Text style={[styles.textbuttonGoogle]}>NaCl, Whiteness and Water Content</Text>
                                </Col>
                                <Col style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                    <Image style={styles.itemGoogleImage} source={require('../assets/icons/retrievedata/searchblue.png')} />
                                </Col>
                            </Row>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.buttonGoogle]} onPress={() => navigate('ContainDetailIodiumScreen')}>
                            <Row>
                                <Col size={4} style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                    <Text style={[styles.textbuttonGoogle]}>Iodium</Text>
                                </Col>
                                <Col style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                    <Image style={styles.itemGoogleImage} source={require('../assets/icons/retrievedata/searchblue.png')} />
                                </Col>
                            </Row>
                        </TouchableOpacity>
                    </View>
                </Row>
                <Row>
                    <Col style={[styles.col]}>
                        <TouchableOpacity style={[styles.col]} onPress={() => navigate('HomeScreen')}>
                            <Image style={styles.itemMenuImage} source={require('../assets/icons/menubar/homeblue.png')} />
                            <Text style={[styles.textmenu]}>Home</Text>
                        </TouchableOpacity>
                    </Col>
                    <Col style={[styles.col]}>
                        <TouchableOpacity style={[styles.col]} onPress={() => navigate('RetrieveDataScreen')}>
                            <Image style={styles.itemMenuImage} source={require('../assets/icons/menubar/retrieveblue.png')} />
                            <Text style={[styles.textmenu]}>Retrieve Data</Text>
                        </TouchableOpacity>
                    </Col>
                    <Col style={[styles.col]}>
                        <TouchableOpacity style={[styles.col]} onPress={() => navigate('')}>
                            <Image style={styles.itemMenuImage} source={require('../assets/icons/menubar/datablue.png')} />
                            <Text style={[styles.textmenu]}>View Data</Text>
                        </TouchableOpacity>
                    </Col>
                    <Col style={[styles.col]}>
                        <TouchableOpacity style={[styles.col]} onPress={() => navigate('EditProfileScreen')}>
                            <Image style={styles.itemMenuImage} source={require('../assets/icons/menubar/profileblue.png')} />
                            <Text style={[styles.textmenu]}>Profile</Text>
                        </TouchableOpacity>
                    </Col>
                    <Col style={[styles.col]}>
                        <TouchableOpacity style={[styles.col]} onPress={() => navigate('SettingScreen')}>
                            <Image style={styles.itemMenuImage} source={require('../assets/icons/menubar/settingblue.png')} />
                            <Text style={[styles.textmenu]}>Setting</Text>
                        </TouchableOpacity>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

const win = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f3f3f3'
    },
    logo: {
        width: 45,
        height: 45,
    },
    button: {
        marginTop: -130,
        marginBottom: 60,
        marginRight: -320
    },
    text: {
        color: '#fff',
        fontSize: 16,
        marginTop: 15,
        margin: 15,
        textAlign: 'justify',
        color: '#000',
        fontWeight: '600'
    },
    itemMenuImage: {
        resizeMode: 'contain',
        width: 25,
        height: 25,
        marginTop: 3
    },
    col: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    allowleft: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: -30,
        marginBottom: 10
    },
    allowright: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 80,
        marginBottom: 10
    },
    textbuttonallow: {
        fontSize: 17,
        color: '#000',
        textAlign: 'center',
        marginTop: 20
    },
    textmenu: {
        fontSize: 10,
        marginTop: 5,
        color: '#808080',
    },
    textbluetooth: {
        fontSize: 16,
        margin: 29,
        textAlign: 'center',
        color: '#129cd8',
    },
    buttonGoogle: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        width: 330,
        height: 100,
        backgroundColor: '#fff',
        marginBottom: 30
    },
    textbuttonGoogle: {
        fontSize: 20,
        color: '#000',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',

    },
    itemGoogleImage: {
        resizeMode: 'contain',
        width: 35,
        height: 35,
        margin: 20,
        marginTop: 25,
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: -230,
        marginBottom: 80
    },
    itemIconImage: {
        resizeMode: 'contain',
        width: 60,
        height: 60,
    },
    itemText: {
        color: '#129cd8',
        marginLeft: 13,
        fontSize: 25,
        fontWeight: '700'
    },
    textbuttontitle: {
        fontSize: 13,
        color: 'red',
        fontWeight: '600',
        textAlign: 'center',
        margin: 20,
        marginTop: -10
    },
    Border: {
        alignSelf: 'stretch',
        width: 300,
        color: '#808080',
        borderBottomColor: '#808080',
        borderBottomWidth: 1,
    },
    BorderTop: {
        alignSelf: 'stretch',
        width: 300,
        color: '#808080',
        borderBottomColor: '#808080',
        borderBottomWidth: 4,
    },
    buttonsearch: {
        alignSelf: 'center',
        alignItems: 'center',
        borderRadius: 40,
        width: 280,
        height: 50,
        padding: 10,
        backgroundColor: '#129cd8',
        marginTop: 30
    },
    textbuttonsearch: {
        fontSize: 20,
        color: '#fff',
        fontWeight: '700',
        textAlign: 'center',
    },
});
